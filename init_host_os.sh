if [ "$EUID" -ne 0 ]
  then echo "Must run as root"
  exit
fi

apt update
apt upgrade -y

apt-get remove -y docker docker-engine docker.io
apt-get install -y  apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
apt-key fingerprint 0EBFCD88
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update -y
apt-get install -y docker-ce
apt-get install -y  lxc

if [ "vm.max_map_count = 262144" -ne `vm.max_map_count = 262144` ]
then
        echo " vm.max_map_count=262144" >> /etc/sysctl.conf
fi

systemctl enable docker
systemctl enable lxc
systemctl enable lxc-net.service

reboot
