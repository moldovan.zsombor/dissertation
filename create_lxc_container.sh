lxc-create -t download -n test001 -- --dist ubuntu --release bionic --arch amd64
lxc-start -n test001
sleep 1 
lxc-attach -n test001 -- apt update 
lxc-attach -n test001 -- apt upgrade
lxc-attach -n test001 -- apt install -y default-jre
lxc-attach -n test001 -- apt install -y wget 
lxc-attach -n test001 -- wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.5.2.deb
lxc-attach -n test001 -- dpkg -i elasticsearch-6.5.2.deb
lxc-attach -n test001 -- echo "network.host: 0.0.0.0" >> /etc/elasticsearch/elasticsearch.yml
lxc-attach -n test001 -- systemctl start elasticsearch  