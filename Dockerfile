#Verzio 1.0
#Készítő : Moldován Zsombor Fülöp
FROM java:8

RUN groupadd -g 1000 elasticsearch && useradd elasticsearch -u 1000 -g 1000 \
    && mkdir -p /home/elasticsearch/ \
    && chown elasticsearch:elasticsearch -R /home/elasticsearch \
    && mkdir -p /data \
    && chown elasticsearch:elasticsearch -R /data


VOLUME ["/data"]

USER elasticsearch

#Elasticsearch telelpítése
RUN cd /home/elasticsearch/ \
    && curl -s https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.5.1.tar.gz | tar xvz \
    && mv elasticsearch-6.5.1 elasticsearch

COPY --chown=elasticsearch config/elasticsearch.yml /home/elasticsearch/elasticsearch/config/elasticsearch.yml


ENV PATH=$PATH:/home/elasticsearch/elasticsearch/bin

CMD ["elasticsearch"]

EXPOSE 9200 9300

