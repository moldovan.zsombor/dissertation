while : ; do
        echo "Wait"
        sleep 1
        curl -s $1 >> /dev/null
    [[ $? != 0 ]] || break
done

sleep 10
if [ ! -f ./shakespeare_6.0.json ]; then
    wget https://download.elastic.co/demos/kibana/gettingstarted/shakespeare_6.0.json
    curl -X PUT "${1}/shakespeare" -H 'Content-Type: application/json' -d'
    {
     "mappings": {
      "doc": {
       "properties": {
        "speaker": {"type": "keyword"},
        "play_name": {"type": "keyword"},
        "line_id": {"type": "integer"},
        "speech_number": {"type": "integer"}
       }
      }
     }
    }
    '
fi

curl -sH 'Content-Type: application/x-ndjson' -XPOST "${1}/shakespeare/doc/_bulk?pretty" --data-binary @demofile.json
sleep 1

python3 main.py --url $1 -f 10 1 1 5 2