import asyncio
import csv
import json
import sys
import time
from functools import reduce

import requests


def find_and_wrap(map_to_array, arguments, name):
    try:
        index_of_the_url = arguments.index(name)
        if isinstance(map_to_array, list):
            return list(map(lambda mapper: try_map(mapper, arguments, index_of_the_url), map_to_array))
        else:
            return [try_map(map_to_array, arguments, index_of_the_url)]
    except ValueError:
        return []


def try_map(mapper, arguments, index_of_the_url):
    try:
        return mapper(arguments, index_of_the_url)
    except IndexError:
        return


def url_parser(arguments):
    return find_and_wrap(lambda array, index: ['url', array[index + 1]], arguments, '--url')


def frequency_parser(arguments):
    try:
        return find_and_wrap([lambda array, index: ['frequency', array[index + 1]],
                              lambda array, index: when_numeric(array[index + 2], ['frequency_from', array[index + 2]]),
                              lambda array, index: when_numeric(array[index + 4], ['frequency_step', array[index + 2]])],
                             arguments, '-f')
    except ValueError:
        return []


def when_numeric(value, data):
    if value.isnumeric():
        return data
    else:
        return []


def csv_path_parser(arguments):
    return find_and_wrap(lambda array, index: ['csv_path', array[index + 1]], arguments, '--output')


def argument_parser(arguments):
    return dict((key, value) for key, value in [x for x in url_parser(arguments) + frequency_parser(arguments) + csv_path_parser(arguments) if x is not None])


default_query = json.dumps({
    "query": {
        "match_all": {}

    }
})

parameters = argument_parser(sys.argv)


async def send_request(url):
    get = requests.get(url)
    if get.status_code < 200 or get.status_code > 299:
        print(get.json())
        print(get.status_code)
        raise Exception("Bad request")
    return {"client": get.elapsed.total_seconds(), 'elastic': get.json()['took']}


def integral(array, step):
    return list(map(lambda x: reduce(lambda a, b: a + b, x) / step, chunk(array, step)))


def chunk(array, step):
    return [array[i:i + step] for i in range(0, len(array), step)]


def get_or_default(from_, what, default):
    try:
        if from_[what] is None:
            return default
        else:
            return from_[what]
    except KeyError:
        return default


async def measure():
    url = get_or_default(parameters, 'url', 'http://localhost:9200')
    frequency = int(get_or_default(parameters, 'frequency', '120'))
    frequency_from = int(get_or_default(parameters, 'frequency_from', '1'))
    frequency_step = int(get_or_default(parameters, 'frequency_step', '1'))
    csv_path = get_or_default(parameters, 'csv_path', 'STDOUT')

    actual = frequency_from
    done, pending = await asyncio.wait([send_requests(actual, frequency, frequency_step, url)])

    result = done.pop().result()
    print(result)
    with open(csv_path, 'w', newline='') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow(['Actual', 'Delay', 'Avg client', 'Avg  Elasticshearc'])
        for i in result:
            values = i['values']
            spamwriter.writerow([i['actual'], i['all_elapsed_ms'], integral(list(map(lambda x: x['client'], values)), len(values))[0],
                                 integral(list(map(lambda x: x['elastic'], values)), len(values))[0]])


async def send_requests(actual, frequency, frequency_step, url):
    results = []
    for i in range(0, frequency):
        print("We are in {:.1%}".format(i / frequency))
        start = int(round(time.time() * 1000))
        futures = [send_request(url + '/shakespeare/_search?size=1000') for _ in range(actual)]
        done, pending = await asyncio.wait(futures)
        stop = int(round(time.time() * 1000))
        actual += frequency_step

        results.append({
            'values': [s.result() for s in done],
            'all_elapsed_ms': stop - start,
            'actual': actual
        })
    return results


ioloop = asyncio.get_event_loop()
ioloop.run_until_complete(measure())
ioloop.close()
