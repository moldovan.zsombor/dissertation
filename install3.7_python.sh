if [ "$EUID" -ne 0 ]
  then echo "Must run as root"
  exit
fi

apt install -y python3.7